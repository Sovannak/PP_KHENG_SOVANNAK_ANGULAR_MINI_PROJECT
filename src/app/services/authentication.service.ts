import { Injectable } from '@angular/core';
import { IAuthentication } from '../models/iauthentication';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor() {}

  login(authentication: IAuthentication): boolean {
    if (authentication) {
      localStorage.setItem('authentication', JSON.stringify(authentication));
      return true;
    }
    return false;
  }
}
