import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IBook } from '../models/ibook';

@Injectable({
  providedIn: 'root',
})
export class BookService {
  private bookUrl: string = 'api/books/';
  constructor(private _httpClient: HttpClient) {}

  getBooks(): Observable<IBook[]> {
    return this._httpClient.get<IBook[]>(this.bookUrl);
  }

  postBook(book: IBook): Observable<IBook> {
    return this._httpClient.post<IBook>(this.bookUrl, book);
  }

  deleteBook(book: IBook): Observable<IBook> {
    return this._httpClient.delete<IBook>(this.bookUrl + book.id);
  }

  putBook(book: IBook): Observable<IBook> {
    return this._httpClient.put<IBook>(this.bookUrl + book.id, book);
  }
}
