import { Injectable } from '@angular/core';
import { ICategory } from '../models/icategory';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  constructor() {}

  categories: ICategory[] = [
    {
      id: 1,
      name: 'Business',
    },
    {
      id: 2,
      name: 'Science',
    },
    {
      id: 3,
      name: 'Fiction',
    },
    {
      id: 4,
      name: 'Philosophy',
    },
    {
      id: 5,
      name: 'Biography',
    },
  ];
}
