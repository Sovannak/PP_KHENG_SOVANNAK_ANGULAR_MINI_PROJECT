import { Injectable } from '@angular/core';
import { InMemoryDbService, RequestInfo } from 'angular-in-memory-web-api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BookMockupService implements InMemoryDbService {
  constructor() {}

  createDb() {
    return {
      books: [
        {
          id: 1,
          title: 'Long Brigth River',
          description:
            'Many variations of passages of Lorem Ipsum willing araise alteration in some form.',
          image:
            'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTI1XY0MjMRlJPGEuFrni2CDBa3-6pEJ3hX6giXSpzs-1nF3zBj',
          author: 'Jonh Abraham',
          category: 'Science',
        },
        {
          id: 2,
          title: 'Garis Waktu',
          description:
            'Many variations of passages of Lorem Ipsum willing araise alteration in some form.',
          image:
            'https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1471933385i/31604219.jpg',
          author: 'Fiersa Besari',
          category: 'Science',
        },
      ],
    };
  }
}
