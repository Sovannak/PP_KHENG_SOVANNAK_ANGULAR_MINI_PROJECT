import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { BookComponent } from './components/book/book.component';
import { authenticationGuard } from './guards/authentication.guard';
import { HomeComponent } from './components/home/home.component';
import { AddBookComponent } from './components/add-book/add-book.component';
import { ViewBookComponent } from './components/view-book/view-book.component';
import { FilterBookCategoryComponent } from './components/filter-book-category/filter-book-category.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'book',
    component: BookComponent,
    canActivate: [authenticationGuard],
    children: [
      {
        path: 'filterBookCategory',
        component: FilterBookCategoryComponent,
      },
    ],
  },
  {
    path: 'addbook',
    component: AddBookComponent,
    canActivate: [authenticationGuard],
  },
  {
    path: 'book/:id',
    component: ViewBookComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
