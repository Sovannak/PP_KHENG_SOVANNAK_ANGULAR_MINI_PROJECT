import { Directive } from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';

@Directive({
  selector: '[appValidationPassword]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ValidationPasswordDirective,
      multi: true,
    },
  ],
})
export class ValidationPasswordDirective implements Validator {
  constructor() {}

  validate(control: AbstractControl): ValidationErrors | null {
    const regexPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/;
    if (!regexPassword.test(control.value)) {
      return { passwordIsRestricted: true };
    }
    return null;
  }
}
