import { Directive } from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms';

@Directive({
  selector: '[appValidationEmail]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ValidationEmailDirective,
      multi: true,
    },
  ],
})
export class ValidationEmailDirective implements Validator {
  constructor() {}
  validate(control: AbstractControl): ValidationErrors | null {
    const regexEmail = /^[A-Za-z]+_\d{3}@gmail\.com$/;
    if (!regexEmail.test(control.value)) {
      return { emailIsRestricted: true };
    }
    return null;
  }
}
