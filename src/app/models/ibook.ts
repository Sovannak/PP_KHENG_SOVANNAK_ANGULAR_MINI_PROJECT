export interface IBook {
  id: number;
  title: string;
  description: string;
  image: string;
  author: string;
  category: string;
}
