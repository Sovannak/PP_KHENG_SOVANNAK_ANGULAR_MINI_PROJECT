import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { IBook } from 'src/app/models/ibook';
import { BookService } from 'src/app/services/book.service';
import { EditbookComponent } from '../editbook/editbook.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  constructor(private _bookService: BookService, private _router: Router, public dialog: MatDialog) {}

  books!: IBook[];

  ngOnInit(): void {
    this.fetchBooks();
  }

  openDialog(book: IBook) {
    const dialogRef = this.dialog.open(EditbookComponent, {
      data: { ...book },
    });
    dialogRef.afterClosed().subscribe((res) => this.fetchBooks());
  }

  fetchBooks(): void {
    this._bookService.getBooks().subscribe({
      next: (res) => (this.books = res),
      error: (err) => console.log(err),
    });
  }

  removeBook(book: IBook): void {
    this._bookService.deleteBook(book).subscribe({
      next: (res) => console.log(res),
      error: (err) => console.log(err),
    });
    this.fetchBooks();
  }

  viewBook(id: number): void {
    for (let book of this.books) {
      if (book.id === id) {
        this._router.navigate(['/book', book.id]);
      }
    }
  }
}
