import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-editbook',
  templateUrl: './editbook.component.html',
  styleUrls: ['./editbook.component.css'],
})
export class EditbookComponent implements OnInit {
  constructor(
    private _bookService: BookService,
    @Inject(MAT_DIALOG_DATA) public book: any
  ) {}

  reactiveEditBookForm!: FormGroup;

  ngOnInit(): void {
    this.reactiveEditBookForm = new FormGroup({
      title: new FormControl(null, Validators.required),
      author: new FormControl(null, Validators.required),
      description: new FormControl(null, Validators.required),
    });
    this.setDefaultBook();
  }

  setDefaultBook(): void {
    this.reactiveEditBookForm.setValue({
      title: this.book.title,
      author: this.book.author,
      description: this.book.description,
    });
  }

  handleUpdateBook(): void {
    this._bookService
      .putBook({
        ...this.reactiveEditBookForm.value,
        id: this.book.id,
        image: this.book.image,
      })
      .subscribe({
        next: (res) => console.log(res),
        error: (err) => console.log(err),
      });
  }
}
