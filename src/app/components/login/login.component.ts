import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  isEye: boolean = false;
  reactiveLoginForm!: FormGroup;

  constructor(
    private _authenticationService: AuthenticationService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.reactiveLoginForm = new FormGroup({
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });
  }


  handleLogin(): void {
    const isSuccess = this._authenticationService.login(
      this.reactiveLoginForm.value
    );
    if (isSuccess) {
      this._router.navigate(['/book']);
    } else {
      this._router.navigate(['/login']);
    }
  }
}
