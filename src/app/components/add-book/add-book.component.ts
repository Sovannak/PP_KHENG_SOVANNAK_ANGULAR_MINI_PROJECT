import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ICategory } from 'src/app/models/icategory';
import { BookService } from 'src/app/services/book.service';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css'],
})
export class AddBookComponent implements OnInit {
  constructor(
    private _router: Router,
    private _categoryService: CategoryService,
    private _bookService: BookService
  ) {}

  categories!: ICategory[];
  reactiveAddBookForm!: FormGroup;

  backToBook(): void {
    this._router.navigate(['/book']);
  }

  ngOnInit(): void {
    this.categories = this._categoryService.categories;
    this.reactiveAddBookForm = new FormGroup({
      title: new FormControl(null, Validators.required),
      author: new FormControl(null, Validators.required),
      category: new FormControl(null, Validators.required),
      description: new FormControl(null, Validators.required),
    });
  }

  handleAddBook(): void {
    this._bookService
      .postBook({
        ...this.reactiveAddBookForm.value,
        image:
          'https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1471933385i/31604219.jpg',
      })
      .subscribe({
        next: (res) => console.log(res),
        error: (err) => console.log(err),
      });
    this._router.navigate(['/book']);
  }
}
