import { Component, DoCheck, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IBook } from 'src/app/models/ibook';
import { BookService } from 'src/app/services/book.service';
import { MatDialog } from '@angular/material/dialog';
import { EditbookComponent } from '../editbook/editbook.component';
import { FormControl, FormGroup } from '@angular/forms';
import { CategoryService } from 'src/app/services/category.service';
import { ICategory } from 'src/app/models/icategory';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css'],
})
export class BookComponent implements OnInit {
  constructor(
    private _router: Router,
    private _bookService: BookService,
    private _categoryService: CategoryService,
    public dialog: MatDialog
  ) {}

  reactiveSearchForm!: FormGroup;
  books!: IBook[];
  categories!: ICategory[];

  openDialog(book: IBook) {
    const dialogRef = this.dialog.open(EditbookComponent, {
      data: { ...book },
    });
    dialogRef.afterClosed().subscribe((res) => this.fetchBooks());
  }

  addBook(): void {
    this._router.navigate(['/addbook']);
  }

  ngOnInit(): void {
    this.fetchBooks();
    this.categories = this._categoryService.categories;
    this.reactiveSearchForm = new FormGroup({
      search: new FormControl(''),
    });
  }

  isFilterCategory(): boolean {
    return this._router.url === '/book';
  }

  fetchBooks(): void {
    this._bookService.getBooks().subscribe({
      next: (res) => (this.books = res),
      error: (err) => console.log(err),
    });
  }

  removeBook(book: IBook): void {
    this._bookService.deleteBook(book).subscribe({
      next: (res) => console.log(res),
      error: (err) => console.log(err),
    });
    this.fetchBooks();
  }

  viewBook(id: number): void {
    for (let book of this.books) {
      if (book.id === id) {
        this._router.navigate(['/book', book.id]);
      }
    }
  }
}
