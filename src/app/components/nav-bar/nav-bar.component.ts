import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css'],
})
export class NavBarComponent implements OnInit {
  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) {}

  queryParam: string | undefined | null;

  goToLogin(): void {
    this._router.navigate(['/login']);
  }

  goToHome(): void {
    this._router.navigate(['/']);
    localStorage.removeItem('authentication');
  }

  ngOnInit(): void {
    this._activatedRoute.queryParamMap.subscribe((queryParam) => {
      this.queryParam = queryParam.get('categoryName');
    });
  }

  isBookUrl(): boolean {
    return (
      this._router.url === '/book' ||
      this._router.url ===
        `/book/filterBookCategory?categoryName=${this.queryParam}`
    );
  }
}
