import { Component, DoCheck, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IBook } from 'src/app/models/ibook';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-filter-book-category',
  templateUrl: './filter-book-category.component.html',
  styleUrls: ['./filter-book-category.component.css'],
})
export class FilterBookCategoryComponent implements OnInit {
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _bookService: BookService,
    private _router: Router
  ) {}

  books!: IBook[];
  queryParam!: string | null;

  ngOnInit(): void {
    this._activatedRoute.queryParamMap.subscribe((queryParam) => {
      this.queryParam = queryParam.get('categoryName');
      this.getCategoryFormQueryParamMap();
    });
  }

  getCategoryFormQueryParamMap(): void {
    this._bookService.getBooks().subscribe({
      next: (res) => {
        this.books = res.filter((val) => val.category === this.queryParam);
      },
      error: (err) => console.log(err),
    });
  }

  viewBook(id: number): void {
    for (let book of this.books) {
      if (book.id === id) {
        this._router.navigate(['/book', book.id]);
      }
    }
  }
}
