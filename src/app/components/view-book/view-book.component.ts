import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IBook } from 'src/app/models/ibook';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-view-book',
  templateUrl: './view-book.component.html',
  styleUrls: ['./view-book.component.css'],
})
export class ViewBookComponent implements OnInit {
  constructor(
    private _router: Router,
    private _bookService: BookService,
    private _activaedRoute: ActivatedRoute
  ) {}

  book!: IBook | undefined;
  id!: number;

  ngOnInit(): void {
    this.getBookFromUrl();
  }

  backToBook(): void {
    this._router.navigate(['/']);
  }

  getBookFromUrl(): void {
    this._activaedRoute.paramMap.subscribe(
      (res) => (this.id = +res.get('id')!)
    );
    this._bookService.getBooks().subscribe({
      next: (res) => (this.book = res.find((val) => val.id === this.id)),
      error: (err) => console.log(err),
    });
  }
}
