import { Pipe, PipeTransform } from '@angular/core';
import { ICategory } from '../models/icategory';

@Pipe({
  name: 'search',
})
export class SearchPipe implements PipeTransform {
  transform(value: ICategory[], search: string): ICategory[] {
    return value.filter((item) =>
      item.name.toLowerCase().includes(search.toLowerCase())
    );
  }
}
