import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { AddBookComponent } from './components/add-book/add-book.component';
import { ViewBookComponent } from './components/view-book/view-book.component';
import { EmailValidator, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { EditbookComponent } from './components/editbook/editbook.component';
import { LoginComponent } from './components/login/login.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { BookComponent } from './components/book/book.component';
import { HttpClientModule } from '@angular/common/http';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { BookMockupService } from './services/book-mockup.service';
import { SearchPipe } from './pipes/search.pipe';
import { FilterBookCategoryComponent } from './components/filter-book-category/filter-book-category.component';
import { ValidationEmailDirective } from './directives/validation-email.directive';
import { ValidationPasswordDirective } from './directives/validation-password.directive';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddBookComponent,
    ViewBookComponent,
    EditbookComponent,
    LoginComponent,
    HomeComponent,
    NavBarComponent,
    BookComponent,
    SearchPipe,
    FilterBookCategoryComponent,
    ValidationEmailDirective,
    ValidationPasswordDirective,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatDialogModule,
    HttpClientModule,
    InMemoryWebApiModule.forRoot(BookMockupService)
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
