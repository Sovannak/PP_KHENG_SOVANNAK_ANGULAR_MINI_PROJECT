/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,ts}"],
  theme: {
    extend: {
      colors: {
        primary: "#1B3764",
        'primary-text' : '#343434'
      },
    },
  },
  plugins: [],
};
